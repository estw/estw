﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ReactiveUI;

namespace ESTW.Elements.Lupe
{
    /// <summary>
    /// Interaktionslogik für KsSignal.xaml
    /// </summary>
    public partial class KsSignal : UserControl, IViewFor<KsSignalViewModel>
    {
        public KsSignal()
        {
            InitializeComponent();

            DataContext = ViewModel = new KsSignalViewModel();
            //this.Bind(ViewModel, x => x.IsHsigVisible, HSig);
        }

        public KsSignalViewModel ViewModel
        {
            get { return (KsSignalViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof (KsSignalViewModel), typeof (KsSignal),
                new PropertyMetadata(null));

        object IViewFor.ViewModel
        {
            get { return ViewModel; }
            set { ViewModel = (KsSignalViewModel) value; }
        }
    }
}
