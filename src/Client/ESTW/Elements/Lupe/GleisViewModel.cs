﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using ESTW.Client.Models.Gleis;
using ReactiveUI;

namespace ESTW.Elements.Lupe
{
    public class GleisViewModel : ReactiveObject
    {
        private readonly ObservableAsPropertyHelper<Brush> _gleisBrush;

        public Brush GleisBrush
        {
            get { return _gleisBrush.Value; }
        }

        private Gleis _model;

        public Gleis Model
        {
            get { return _model; }
            set { this.RaiseAndSetIfChanged(ref _model, value); }
        }

        public GleisViewModel()
        {
            this.WhenAnyValue(x => x.Model.Besetztstatus)
                .Select(GetBrushFromStatus)
                .ToProperty(this, x => x.GleisBrush, out _gleisBrush);

            Model = new Gleis();
        }

        private Brush GetBrushFromStatus(Besetztstatus status)
        {
            if(status.HasFlag(Besetztstatus.Belegt))
                return Brushes.Red;

            if (status.HasFlag(Besetztstatus.Fahrstrasse))
                return Brushes.Green;

            if (status.HasFlag(Besetztstatus.Rangiestrasse))
                return Brushes.Blue;

            if (Model.FreiWeiss)
                return Brushes.White;
            return Brushes.Yellow;
        }
    }
}
