﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ReactiveUI;

namespace ESTW.Elements.Lupe
{
    /// <summary>
    /// Interaktionslogik für Gerade.xaml
    /// </summary>
    public partial class Gerade : UserControl, IViewFor<GleisViewModel>
    {
        public Gerade()
        {
            InitializeComponent();
            ViewModel = new GleisViewModel();
        }


        public GleisViewModel ViewModel
        {
            get { return (GleisViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof (GleisViewModel), typeof (Gerade),
                new PropertyMetadata(null));

        object IViewFor.ViewModel
        {
            get { return ViewModel; }
            set { ViewModel = (GleisViewModel) value; }
        }
    }
}
