﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ESTW.ViewModels;
using ReactiveUI;

namespace ESTW.Elements.Lupe
{
    /// <summary>
    /// Interaktionslogik für Gerade.xaml
    /// </summary>
    public partial class WeicheLi : UserControl, IViewFor<WeicheViewModel>
    {
        public WeicheLi()
        {
            InitializeComponent();
            ViewModel = new WeicheViewModel();
        }


        public WeicheViewModel ViewModel
        {
            get { return (WeicheViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(WeicheViewModel), typeof(WeicheLi),
                new PropertyMetadata(null));

        object IViewFor.ViewModel
        {
            get { return ViewModel; }
            set { ViewModel = (WeicheViewModel)value; }
        }
    }
}
