﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using ESTW.Client.Models.Signals;
using ReactiveUI;

namespace ESTW.Elements.Lupe
{
    public class KsSignalViewModel : ReactiveObject
    {
        private readonly ObservableAsPropertyHelper<bool> _isHsigVisible;
        private bool _ersatzsignalModus;
        public bool IsHsigVisible
        {
            get { return _isHsigVisible.Value; }
        }

        private bool _isHsigOnlySchirmVisible;
        public bool IsHsigOnlySchirmVisible
        {
            get { return _isHsigOnlySchirmVisible; }
            set { this.RaiseAndSetIfChanged(ref _isHsigOnlySchirmVisible, value); }
        }

        private bool _isSchirmBotVisible = true;
        public bool IsSchirmBotVisible
        {
            get { return _isSchirmBotVisible; }
            set { this.RaiseAndSetIfChanged(ref _isSchirmBotVisible, value); }
        }

        private bool _isVsigVisible;
        public bool IsVsigVisible
        {
            get { return _isVsigVisible; }
            set { this.RaiseAndSetIfChanged(ref _isVsigVisible, value); }
        }

        private bool _isZs7Visible = false;
        public bool IsZs7Visible
        {
            get { return _isZs7Visible; }
            set { this.RaiseAndSetIfChanged(ref _isZs7Visible, value); }
        }

        public KsSignalViewModel()
        {
            MastBrush = SchirmBrush = Brushes.Cyan;
            Model = new Client.Models.Signals.KsSignal();

            // HSig
            this.WhenAnyValue(x => x.Model.IsHsig)
                .ToProperty(this, x => x.IsHsigVisible, out _isHsigVisible);

            //// VSig
            //this.WhenAnyValue(x => x.Model.IsVsig)
            //    .ToProperty(this, x => x.IsVsigVisible, out _isVsigVisible);

            // VSig
            this.WhenAnyValue(x => x.Model.IsVsig)
                .Subscribe(x =>
                {
                    IsVsigVisible = x;
                });

            //Signalbegriffe
            this.WhenAnyValue(x => x.Model.Signalbegriff)
                .SubscribeOn(RxApp.MainThreadScheduler)
                .Subscribe(UpdateBrushes);

            //Verkuerzt
            this.WhenAnyValue(x => x.Model.Verkuerzt)
                .SubscribeOn(RxApp.MainThreadScheduler)
                .Subscribe(UpdateVerkuerzt);
        }

        private void UpdateBrushes(Signalbegriff signalbegriff)
        {
            if (_ersatzsignalModus && (signalbegriff != Signalbegriff.Zs1 || signalbegriff != Signalbegriff.Zs7))
            {
                _ersatzsignalModus = false;
                IsSchirmBotVisible = true;
                IsVsigVisible = Model.IsVsig;
                IsHsigOnlySchirmVisible = !Model.IsVsig;
            }

            switch (signalbegriff)
            {
                case Signalbegriff.Hp0:
                    VsigBrush = SchirmBrush = MastBrush = Brushes.Red;
                    return;
                case Signalbegriff.Ks1:
                    SchirmBrush = Brushes.Green;
                    break;
                case Signalbegriff.Ks2:
                    SchirmBrush = Brushes.Yellow;
                    break;
                case Signalbegriff.Zs7:
                    IsVsigVisible = IsHsigOnlySchirmVisible = IsSchirmBotVisible = false;
                    SchirmBrush = MastBrush = Brushes.Red;
                    IsZs7Visible = true;
                    _ersatzsignalModus = true;
                    return;
                case Signalbegriff.Zs1:
                    SchirmBrush = MastBrush = Brushes.Red;
                    VsigBrush = Brushes.White;
                    IsVsigVisible = true;
                    IsHsigOnlySchirmVisible = IsZs7Visible = IsSchirmBotVisible = false;
                    _ersatzsignalModus = true;
                    return;
                case Signalbegriff.Sh1:
                    SchirmBrush = MastBrush = Brushes.Red;
                    VsigBrush = Brushes.White;
                    IsVsigVisible = false;
                    IsHsigOnlySchirmVisible = true;
                    _ersatzsignalModus = true;
                    return;
                default:
                    SchirmBrush = Brushes.Cyan;
                    break;
            }

            VsigBrush = SchirmBrush;
            MastBrush = Model.Verkuerzt ? Brushes.White : SchirmBrush;
        }

        private void UpdateVerkuerzt(bool verkuerzt)
        {
            if (Model.Signalbegriff == Signalbegriff.Hp0 || Model.Signalbegriff == Signalbegriff.Zs1 ||
                Model.Signalbegriff == Signalbegriff.Zs7 || Model.Signalbegriff == Signalbegriff.Sh1)
                return;
            MastBrush = verkuerzt ? Brushes.White : SchirmBrush;
        }

        private Brush _schirmBrush;
        public Brush SchirmBrush
        {
            get { return _schirmBrush; }
            set { this.RaiseAndSetIfChanged(ref _schirmBrush, value); }
        }

        private Brush _mastBrush;
        public Brush MastBrush
        {
            get { return _mastBrush; }
            set { this.RaiseAndSetIfChanged(ref _mastBrush, value); }
        }

        private Brush _vsigBrush;
        public Brush VsigBrush
        {
            get { return _vsigBrush; }
            set { this.RaiseAndSetIfChanged(ref _vsigBrush, value); }
        }

        private Client.Models.Signals.KsSignal _model;

        public Client.Models.Signals.KsSignal Model
        {
            get { return _model; }
            set { this.RaiseAndSetIfChanged(ref _model, value); }
        }
    }
}
