﻿using ESTW.Elements.Lupe;

namespace ESTW.ViewModels
{
    public class ViewModelLocator : Client.ViewModel.ViewModelLocator
    {
        public KsSignalViewModel KsSignal
        {
            get { return new KsSignalViewModel(); }
        }

        public GleisViewModel Gleis
        {
            get { return new GleisViewModel(); }
        }
    }
}
