﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using ReactiveUI;

namespace ESTW.ViewModels
{
    public class MakeItBlinkTm
    {
        public MakeItBlinkTm()
        {
            var obs = Observable.Interval(TimeSpan.FromSeconds(0.5)).Select(x => new BlinkArgs((int) x));
            MessageBus.Current.RegisterMessageSource(obs);
        }
    }

    public struct BlinkArgs
    {
        public BlinkArgs(int interval)
        {
            State = interval%2 == 0;
            Interval = interval;
        }

        public bool State;
        public int Interval;
    }
}