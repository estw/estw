﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESTW.Client.Models.Gleis
{
    [Flags]
    public enum Besetztstatus
    {
        Frei = 1,
        Belegt = 2,
        Fahrstrasse = 4,
        Rangiestrasse = 8
    }
}
