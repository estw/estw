﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReactiveUI;

namespace ESTW.Client.Models.Gleis
{
    public class Gleis : ReactiveObject
    {
        public Gleis()
        {
            _besetztstatus = Besetztstatus.Frei;
        }

        private Besetztstatus _besetztstatus;

        public Besetztstatus Besetztstatus
        {
            get { return _besetztstatus; }
            set { this.RaiseAndSetIfChanged(ref _besetztstatus, value); }
        }


        private bool _freiWeiss;

        public bool FreiWeiss
        {
            get { return _freiWeiss; }
            set { this.RaiseAndSetIfChanged(ref _freiWeiss, value); }
        }
    }
}
