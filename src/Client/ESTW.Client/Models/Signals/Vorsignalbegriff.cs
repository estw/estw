﻿namespace ESTW.Client.Models.Signals
{
    public enum Vorsignalbegriff
    {
        Vr0,
        Vr1,
        Vr2,
        Ke,
    }
}
