﻿using ReactiveUI;

namespace ESTW.Client.Models.Signals
{
    public abstract class SignalBase : ReactiveObject
    {
        private bool _isHsig;
        public bool IsHsig
        {
            get { return _isHsig; }
            set { this.RaiseAndSetIfChanged(ref _isHsig, value); }
        }

        private bool _isVsig;
        public bool IsVsig
        {
            get { return _isVsig; }
            set { this.RaiseAndSetIfChanged(ref _isVsig, value); }
        }

        private bool _isRsig;
        public bool IsRsig
        {
            get { return _isRsig; }
            set { this.RaiseAndSetIfChanged(ref _isRsig, value); }
        }

        // ReSharper disable once InconsistentNaming
        internal Signalbegriff _signalbegriff;
        public Signalbegriff Signalbegriff
        {
            get { return _signalbegriff; }
            set { this.RaiseAndSetIfChanged(ref _signalbegriff, value); }
        }

        private bool _verkuerzt;
        public bool Verkuerzt
        {
            get { return _verkuerzt; }
            set { this.RaiseAndSetIfChanged(ref _verkuerzt, value); }
        }

        private bool _leftToRight;

        public bool LeftToRight
        {
            get { return _leftToRight; }
            set
            {
                this.RaiseAndSetIfChanged(ref _leftToRight, value);
                this.RaisePropertyChanged("RotateTransformAngle");
            }
        }

        public double RotateTransformAngle
        {
            get
            {
                if (LeftToRight)
                    return -180;
                return 0;
            }
        }
    }
}
