﻿using System.ComponentModel;
using System.Runtime.InteropServices;
using ReactiveUI;

namespace ESTW.Client.Models.Signals
{
    public class KsSignal : SignalBase
    {
        public KsSignal()
        {
            Signalbegriff = Signalbegriff.Hp0;
            IsRsig = false;
            IsHsig = true;
            IsVsig = true;
            Verkuerzt = false;
            LeftToRight = false;

            //var source = Observable.Interval(TimeSpan.FromSeconds(1)).Take(99999);
            //source.Subscribe(x =>
            //{
            //    var y = Convert.ToInt32(x);
            //    y %= 8;
            //    switch (y)
            //    {
            //        case 0:
            //            Signalbegriff = Signalbegriff.Hp0;
            //            break;
            //        case 1:
            //            Signalbegriff = Signalbegriff.Ks2;
            //            Verkuerzt = true;
            //            break;
            //        case 2:
            //            Signalbegriff = Signalbegriff.Ks1;
            //            break;
            //        case 3:
            //            Signalbegriff = Signalbegriff.Ks1;
            //            Verkuerzt = false;
            //            break;
            //        case 4:
            //            Signalbegriff = Signalbegriff.Hp0;
            //            break;
            //        case 5:
            //            Signalbegriff = Signalbegriff.Zs1;
            //            break;
            //        case 6:
            //            Signalbegriff = Signalbegriff.Zs7;
            //            break;
            //        case 7:
            //            Signalbegriff = Signalbegriff.Sh1;
            //            break;
            //    }
            //});
        }

        //TODO: Unterscheiden zwischen HSig, RSig und VSig.
        public new Signalbegriff Signalbegriff
        {
            get { return _signalbegriff; }
            set
            {
                if (value == Signalbegriff.Hp0 ||
                    value == Signalbegriff.Ks1 ||
                    value == Signalbegriff.Ks2 ||
                    value == Signalbegriff.Sh1 ||
                    value == Signalbegriff.Zs1 ||
                    value == Signalbegriff.Ke  ||
                    value == Signalbegriff.Zs7)
                    this.RaiseAndSetIfChanged(ref _signalbegriff, value);
                else
                {
                    throw new InvalidEnumArgumentException("Signalbegriff nicht Unterstützt. Unterstützt wird Hp0, Ks1, Ks2, Sh1, Zs1, Ke, Zs7");
                }
            }
        }
    }
}
