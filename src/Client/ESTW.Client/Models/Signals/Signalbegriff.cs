﻿namespace ESTW.Client.Models.Signals
{
    // ReSharper disable InconsistentNaming
    public enum Signalbegriff : byte
    {
        Hp0,
        Hp1,
        Hp2,
        Ks1,
        Ks2,
        Ke,
        Sh1,
        Zs7,
        Zs1
    }

    // ReSharper restore InconsistentNaming
}