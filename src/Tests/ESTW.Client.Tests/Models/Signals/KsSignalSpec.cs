﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESTW.Client.Models.Signals;
using ESTW.Elements.Lupe;
using Machine.Specifications;
using Machine.Specifications.Model;

namespace ESTW.Client.Tests.Models.Signals
{
    [Subject("KsSignal")]
    public class When_changing_the_Model
    {
        Establish context = () =>
        {
            Subject = new KsSignalViewModel();
        };

        Cleanup after = () =>
        {
            Subject = null;
        };

        Because of = () => Subject.Model = new Client.Models.Signals.KsSignal();

        It should_has_hp0 = () => Subject.Model.Signalbegriff.ShouldEqual(Signalbegriff.Hp0);

        static KsSignalViewModel Subject;
    }
}
