#!/bin/bash

if [ ! -f tools/nuget.exe ]; then
  wget -P tools/ http://nuget.org/nuget.exe
fi

if [ ! -f packages/FAKE/tools/FAKE.exe ]; then
  mono --runtime=v4.0 tools/nuget.exe install FAKE -OutputDirectory packages -ExcludeVersion
  mono --runtime=v4.0 tools/nuget.exe install FSharp.Formatting.CommandTool -OutputDirectory packages -ExcludeVersion -Prerelease 
  mono --runtime=v4.0 tools/nuget.exe install SourceLink.Fake -OutputDirectory packages -ExcludeVersion 
  mono --runtime=v4.0 tools/nuget.exe install NUnit.Runners -OutputDirectory tools -ExcludeVersion 
  mono --runtime=v4.0 tools/nuget.exe install NUnit -OutputDirectory tools -ExcludeVersion
fi
mono --runtime=v4.0 packages/FAKE/tools/FAKE.exe build.fsx $@