// include Fake lib
#r "tools/FAKE/tools/FakeLib.dll"
open Fake
 
RestorePackages()
 
// Properties
let buildDir = "./build/"
let testDir  = "./test/"
//let deployDir = "./deploy/"
 
// version info
let version = "0.0.1"  // or retrieve from CI server
 
// Targets
Target "Clean" (fun _ ->
    CleanDirs [buildDir; testDir]
)
 
Target "BuildUI" (fun _ ->
   !! @"src/Client/ESTW/*.csproj"
     |> MSBuildRelease buildDir "Build"
     |> Log "UIBuild-Output: "
)

Target "BuildClientLib" (fun _ ->
   !! @"src/Client/ESTW.Client/*.csproj"
     |> MSBuildRelease buildDir "Build"
     |> Log "ClientLib-Output: "
)
 
Target "BuildCommon" (fun _ ->
    !! "src/Common/**/*.csproj"
      |> MSBuildRelease buildDir "Build"
      |> Log "CommonBuild-Output: "
)

Target "BuildTests" (fun _ ->
    !! "src/Tests/**/*.csproj"
      |> MSBuildDebug testDir "Build"
      |> Log "TestBuild-Output: "
)
 
Target "Test" (fun _ ->
    !! (testDir + "/*.Tests.dll") 
      |> NUnit (fun p ->
          {p with
             DisableShadowCopy = true;
             OutputFile = testDir + "TestResults.xml" })
)
 
//Target "Zip" (fun _ ->
//    !! (buildDir + "/**/*.*") 
//        -- "*.zip"
//        |> Zip buildDir (deployDir + "Calculator." + version + ".zip")
//)
 
Target "Default" (fun _ ->
    trace "... Fertig"
)
 
// Dependencies
"Clean"
  ==> "BuildCommon"
  ==> "BuildClientLib"
  =?> ("BuildUI", not isLinux)
  ==> "BuildTests"
  ==> "Test"
  //==> "Zip"
  ==> "Default"
 
// start build
RunTargetOrDefault "Default"